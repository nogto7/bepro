<?
$this->css('detail-more');
$this->js('slider');
$this->css('swiper.min')->css('detail-more')->js('swiper.min')->js('thumb-slider');
?>
<main class="main_wrap">
    <div class="main_block">
        <div class="bed">
            <div class="full_container">
                <div class="breadcrumb">
                    <ul>
                        <li><a href="">Нүүр хуудас</a></li>
                        <li><a href="">Улаанбаатар</a></li>
                        <li><a href="">Зарна</a></li>
                        <li><a href="">Төсөл</a></li>
                        <li><p>River Castle</p></li>
                    </ul>
                </div>
                <div class="b_head dsb">
                    <div class="__head_left">
                        <h1>River Castle</h1>
                        <div class="__addr">Сүхбаатар дүүрэг, 8-р хороо, Чингис хаан зочид буудлын баруун талд</div>
                    </div>
                    <div class="__head_right">
                        <ul>
                            <li><div class="__sf_btn btn __btn_m default"><span class="icon __share_icon"></span>Хуваалцах</div></li>
                            <li><div class="__sf_btn btn __btn_m default"><span class="icon __fav_icon"></span>Хадгалах</div></li>
                        </ul>
                    </div>
                </div>
                <div class="d_sub_menu">
                    <div class="sub_menu">
                        <div><span>Өрөөний сонголт</span></div>
                        <div><span>Ерөнхий төлөвлөгөө</span></div>
                        <div><span>Байршил, тээврийн хүртээмж</span></div>
                        <div><span>Ерөнхий үзүүлэлт</span></div>
                        <div><span>Дэд бүтэц</span></div>
                    </div>
                </div>
            </div>
            <div class="full_container">
                <div class="box_shadow">
                    <div class="dg g_d_s gap2">
                        <div class="d_main">
                            <div class="d_item d_slider oh">
                                <div class="slider_thumbs pa clearfix">
                                    <div class="thumbs">
                                        <div>
                                            <img src="images/d1.jpeg" />
                                        </div>
                                        <div>
                                            <img src="images/d2.jpg" />
                                        </div>
                                        <div>
                                            <img src="images/d3.jpg" />
                                        </div>
                                        <div>
                                            <img src="images/d4.jpg" />
                                        </div>
                                    </div>
                                </div>
                                <div class="big_img __image_box">
                                    <img id="blur" src="images/d1.jpeg">
                                    <div class="__image_box_content">
                                        <img id="bigimg" src="images/d1.jpeg">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="d_list">
                            <div class="dcsp">
                                <div class="">
                                    <label>Орон сууцны ангилал</label>
                                    <h4>Бизнес</h4>
                                </div>
                            <div class="develop_logo"><img src="images/0-1.jpeg" /></div>
                            </div>
                            <div class="dg g2 gap1_6">
                                <div>
                                    <label>Талбай</label>
                                    <h3>41.5-171.8<sup>м2</sup></h3>
                                </div>
                                <div>
                                    <label>Үнэ</label>
                                    <h3>4,5<span>сая</span></h3>
                                </div>
                            </div>
                            <div>
                                <label>Ашиглалтад орох</label>
                                <h5>2023 оны 3 улирал</h5>
                            </div>
                            <div class="agent_box gray_bg">
                                <div class="dg g1_40 gap1 aic">
                                    <div class="dg g50_1 gap1 aic">
                                        <div class="d_p_img"><img src="images/ub-properties-logo.png" /></div>
                                        <div>
                                            <h5>Борлуулалгч</h5>
                                            <a href="company_detail.html">Ub properties LLC</a>
                                        </div>
                                    </div>
                                    <div class="agent_phone dfcc"><span></span><i>Утасны дугаар харах</i></div>
                                </div>
                            </div>
                            <div class="schedule_box">
                                <h6>Цаг товлож орон сууц үзэх</h6>
                                <div class="dg g3 gap1">
                                    <div class="schedule_item">
                                        <p>Мяг</p>
                                        <h4>5</h4>
                                        <p>3-р сар</p>
                                    </div>
                                    <div class="schedule_item active">
                                        <p>Лха</p>
                                        <h4>6</h4>
                                        <p>3-р сар</p>
                                    </div>
                                    <div class="schedule_item">
                                        <p>Пүр</p>
                                        <h4>7</h4>
                                        <p>3-р сар</p>
                                    </div>
                                </div>
                                <div class="__mt1_2">
                                    <button class="btn btn_l primary __w10">Цаг товлох</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="d_main main_content">
                    <div class="dg g_d_c gap2">
                        <div class="d_main_wrap">
                            <div class="dm_box">
                                <div class="real_form">
                                    <div class="dm_sub_title">
                                        <h3>Өрөөний сонголт</h3>
                                        <p>River Castle</p>
                                    </div>
                                    <div class="real_form_content dfc">
                                        <div class="real_form_items dfc">
                                            <div class="rf_item">
                                                <select class="select">
                                                    <option value="1">Бүх барилга</option>
                                                    <option value="2">1-р орц</option>
                                                    <option value="3">2-р орц</option>
                                                    <option value="4">3-р орц</option>
                                                    <option value="5">4-р орц</option>
                                                </select>
                                            </div>
                                            <div class="rf_list">
                                                <div><h4>1</h4><span>өрөө</span></div>
                                                <div><h4>2</h4><span>өрөө</span></div>
                                                <div><h4>3</h4><span>өрөө</span></div>
                                                <div><h4>4</h4><span>өрөө</span></div>
                                            </div>
                                        </div>
                                        <button class="btn btn_l default __ml1">Цэвэрлэх</button>
                                    </div>
                                    <div class="room_layout">
                                        <div class="layout_box">
                                            <div class="layout_line dg g1_30 gap1 aic" id="rooms-1">
                                                <div class="dg g40_1 gap1_6 aic">
                                                    <div class="loyout_icon"><img src="images/icon_layout_studio.svg" /></div>
                                                    <div class="dg g5 gap1 aic">
                                                        <div class="room_c">1 өрөө</div>
                                                        <h3 class="room_area">41.5<sup>м2</sup></h3>
                                                        <h3 class="room_price">187<span>сая</span></h3>
                                                        <div class="room_choose">1<span>загвар</span></div>
                                                        <div class="room_ count">12<span>ш</span></div>
                                                    </div>
                                                </div>
                                                <div class="__arrow dfcc"><span class="icon"><svg width="100%" height="100%" viewBox="0 0 24 24" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xml:space="preserve" style="fill-rule:evenodd;clip-rule:evenodd;stroke-linejoin:round;stroke-miterlimit:1.41421;">
                                                    <path d="M9,21C8.93,20.997 8.913,20.999 8.844,20.988C8.74,20.971 8.639,20.939 8.546,20.891C8.453,20.843 8.367,20.781 8.293,20.707C8.049,20.463 7.947,20.102 8.028,19.767C8.058,19.639 8.114,19.518 8.191,19.412C8.232,19.356 8.246,19.344 8.293,19.293L15.586,12L8.293,4.707L8.24,4.649C8.196,4.595 8.184,4.582 8.147,4.522C8.093,4.433 8.052,4.335 8.028,4.233C8.003,4.132 7.995,4.026 8.003,3.922C8.011,3.817 8.036,3.714 8.076,3.617C8.229,3.248 8.6,3 9,3C9.184,3 9.366,3.051 9.522,3.147C9.591,3.189 9.649,3.239 9.707,3.293L17.707,11.293C17.797,11.39 17.873,11.494 17.924,11.617C18.035,11.885 18.023,12.195 17.891,12.454C17.843,12.549 17.779,12.629 17.707,12.707L9.707,20.707C9.61,20.797 9.506,20.873 9.383,20.924C9.309,20.954 9.234,20.972 9.156,20.988C9.078,20.997 9.079,20.997 9,21Z" style="fill:rgb(192,198,208);fill-rule:nonzero;"/>
                                                </svg></span></div>
                                            </div>
                                            <div class="layout_rooms">
                                                <div class="layout_col dg gap1_6">
                                                    <div class="room_img">
                                                        <div class="room_plan">
                                                            <img src="images/41.png" alt="" />
                                                        </div>
                                                    </div>
                                                    <div class="room_desc">
                                                        <div class="d_list dg g2 gap1_6">
                                                            <div>
                                                                <label>Талбай</label>
                                                                <h3>41.5<sup>м2</sup></h3>
                                                            </div>
                                                            <div>
                                                                <label>Давхар</label>
                                                                <h5>7/26</h5>
                                                            </div>
                                                            <div>
                                                                <label>Үнэ</label>
                                                                <h3>186,750,000</h3>
                                                            </div>
                                                            <div>
                                                                <label>Ашиглалтад орох</label>
                                                                <h5>2023 оны 3 улирал</h5>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="layout_box">
                                            <div class="layout_line dg g1_30 gap1 aic" id="rooms-2">
                                                <div class="dg g40_1 gap1_6 aic">
                                                    <div class="loyout_icon"><img src="images/icon_layout_1_room.svg" /></div>
                                                    <div class="dg g5 gap1 aic">
                                                        <div class="room_c">2 өрөө</div>
                                                        <h3 class="room_area">53.5-66.5<sup>м2</sup></h3>
                                                        <h3 class="room_price">240-300<span>сая</span></h3>
                                                        <div class="room_choose">2<span>загвар</span></div>
                                                        <div class="room_ count">8<span>ш</span></div>
                                                    </div>
                                                </div>
                                                <div class="__arrow dfcc"><span class="icon"><svg width="100%" height="100%" viewBox="0 0 24 24" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xml:space="preserve" style="fill-rule:evenodd;clip-rule:evenodd;stroke-linejoin:round;stroke-miterlimit:1.41421;">
                                                    <path d="M9,21C8.93,20.997 8.913,20.999 8.844,20.988C8.74,20.971 8.639,20.939 8.546,20.891C8.453,20.843 8.367,20.781 8.293,20.707C8.049,20.463 7.947,20.102 8.028,19.767C8.058,19.639 8.114,19.518 8.191,19.412C8.232,19.356 8.246,19.344 8.293,19.293L15.586,12L8.293,4.707L8.24,4.649C8.196,4.595 8.184,4.582 8.147,4.522C8.093,4.433 8.052,4.335 8.028,4.233C8.003,4.132 7.995,4.026 8.003,3.922C8.011,3.817 8.036,3.714 8.076,3.617C8.229,3.248 8.6,3 9,3C9.184,3 9.366,3.051 9.522,3.147C9.591,3.189 9.649,3.239 9.707,3.293L17.707,11.293C17.797,11.39 17.873,11.494 17.924,11.617C18.035,11.885 18.023,12.195 17.891,12.454C17.843,12.549 17.779,12.629 17.707,12.707L9.707,20.707C9.61,20.797 9.506,20.873 9.383,20.924C9.309,20.954 9.234,20.972 9.156,20.988C9.078,20.997 9.079,20.997 9,21Z" style="fill:rgb(192,198,208);fill-rule:nonzero;"/>
                                                </svg></span></div>
                                            </div>
                                            <div class="layout_rooms">
                                                <div class="layout_col dg gap1_6">
                                                    <div class="room_img">
                                                        <div class="room_plan">
                                                            <img src="images/53.png" alt="" />
                                                        </div>
                                                    </div>
                                                    <div class="room_desc">
                                                        <div class="d_list dg g2 gap1_6">
                                                            <div>
                                                                <label>Талбай</label>
                                                                <h3>53.5<sup>м2</sup></h3>
                                                            </div>
                                                            <div>
                                                                <label>Давхар</label>
                                                                <h5>8/26</h5>
                                                            </div>
                                                            <div>
                                                                <label>Үнэ</label>
                                                                <h3>240,750,000</h3>
                                                            </div>
                                                            <div>
                                                                <label>Ашиглалтад орох</label>
                                                                <h5>2023 оны 3 улирал</h5>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="layout_col dg gap1_6">
                                                    <div class="room_img">
                                                        <div class="room_plan">
                                                            <img src="images/66.png" alt="" />
                                                        </div>
                                                    </div>
                                                    <div class="room_desc">
                                                        <div class="d_list dg g2 gap1_6">
                                                            <div>
                                                                <label>Талбай</label>
                                                                <h3>66.5<sup>м2</sup></h3>
                                                            </div>
                                                            <div>
                                                                <label>Давхар</label>
                                                                <h5>8/26</h5>
                                                            </div>
                                                            <div>
                                                                <label>Үнэ</label>
                                                                <h3>299,250,000<span>сая</span></h3>
                                                            </div>
                                                            <div>
                                                                <label>Ашиглалтад орох</label>
                                                                <h5>2023 оны 3 улирал</h5>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="layout_box">
                                            <div class="layout_line dg g1_30 gap1 aic" id="rooms-3">
                                                <div class="dg g40_1 gap1_6 aic">
                                                    <div class="loyout_icon"><img src="images/icon_layout_studio.svg" /></div>
                                                    <div class="dg g5 gap1 aic">
                                                        <div class="room_c">3 өрөө</div>
                                                        <h3 class="room_area">78.0-114.0<sup>м2</sup></h3>
                                                        <h3 class="room_price">351-513<span>сая</span></h3>
                                                        <div class="room_choose">5<span>загвар</span></div>
                                                        <div class="room_ count">12<span>ш</span></div>
                                                    </div>
                                                </div>
                                                <div class="__arrow dfcc"><span class="icon"><svg width="100%" height="100%" viewBox="0 0 24 24" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xml:space="preserve" style="fill-rule:evenodd;clip-rule:evenodd;stroke-linejoin:round;stroke-miterlimit:1.41421;">
                                                    <path d="M9,21C8.93,20.997 8.913,20.999 8.844,20.988C8.74,20.971 8.639,20.939 8.546,20.891C8.453,20.843 8.367,20.781 8.293,20.707C8.049,20.463 7.947,20.102 8.028,19.767C8.058,19.639 8.114,19.518 8.191,19.412C8.232,19.356 8.246,19.344 8.293,19.293L15.586,12L8.293,4.707L8.24,4.649C8.196,4.595 8.184,4.582 8.147,4.522C8.093,4.433 8.052,4.335 8.028,4.233C8.003,4.132 7.995,4.026 8.003,3.922C8.011,3.817 8.036,3.714 8.076,3.617C8.229,3.248 8.6,3 9,3C9.184,3 9.366,3.051 9.522,3.147C9.591,3.189 9.649,3.239 9.707,3.293L17.707,11.293C17.797,11.39 17.873,11.494 17.924,11.617C18.035,11.885 18.023,12.195 17.891,12.454C17.843,12.549 17.779,12.629 17.707,12.707L9.707,20.707C9.61,20.797 9.506,20.873 9.383,20.924C9.309,20.954 9.234,20.972 9.156,20.988C9.078,20.997 9.079,20.997 9,21Z" style="fill:rgb(192,198,208);fill-rule:nonzero;"/>
                                                </svg></span></div>
                                            </div>
                                            <div class="layout_rooms">
                                                <div class="layout_col dg gap1_6">
                                                    <div class="room_img">
                                                        <div class="room_plan">
                                                            <img src="images/78.png" alt="" />
                                                        </div>
                                                    </div>
                                                    <div class="room_desc">
                                                        <div class="d_list dg g2 gap1_6">
                                                            <div>
                                                                <label>Талбай</label>
                                                                <h3>78.0<sup>м2</sup></h3>
                                                            </div>
                                                            <div>
                                                                <label>Давхар</label>
                                                                <h5>9/26</h5>
                                                            </div>
                                                            <div>
                                                                <label>Үнэ</label>
                                                                <h3>351,000,000</h3>
                                                            </div>
                                                            <div>
                                                                <label>Ашиглалтад орох</label>
                                                                <h5>2023 оны 3 улирал</h5>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="layout_col dg gap1_6">
                                                    <div class="room_img">
                                                        <div class="room_plan">
                                                            <img src="images/110.png" alt="" />
                                                        </div>
                                                    </div>
                                                    <div class="room_desc">
                                                        <div class="d_list dg g2 gap1_6">
                                                            <div>
                                                                <label>Талбай</label>
                                                                <h3>110.0<sup>м2</sup></h3>
                                                            </div>
                                                            <div>
                                                                <label>Давхар</label>
                                                                <h5>9/26</h5>
                                                            </div>
                                                            <div>
                                                                <label>Үнэ</label>
                                                                <h3>495,000,000<span>сая</span></h3>
                                                            </div>
                                                            <div>
                                                                <label>Ашиглалтад орох</label>
                                                                <h5>2023 оны 3 улирал</h5>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="layout_col dg gap1_6">
                                                    <div class="room_img">
                                                        <div class="room_plan">
                                                            <img src="images/112.png" alt="" />
                                                        </div>
                                                    </div>
                                                    <div class="room_desc">
                                                        <div class="d_list dg g2 gap1_6">
                                                            <div>
                                                                <label>Талбай</label>
                                                                <h3>112.0<sup>м2</sup></h3>
                                                            </div>
                                                            <div>
                                                                <label>Давхар</label>
                                                                <h5>9/26</h5>
                                                            </div>
                                                            <div>
                                                                <label>Үнэ</label>
                                                                <h3>504,000,000<span>сая</span></h3>
                                                            </div>
                                                            <div>
                                                                <label>Ашиглалтад орох</label>
                                                                <h5>2023 оны 3 улирал</h5>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="layout_col dg gap1_6">
                                                    <div class="room_img">
                                                        <div class="room_plan">
                                                            <img src="images/113.png" alt="" />
                                                        </div>
                                                    </div>
                                                    <div class="room_desc">
                                                        <div class="d_list dg g2 gap1_6">
                                                            <div>
                                                                <label>Талбай</label>
                                                                <h3>112.5<sup>м2</sup></h3>
                                                            </div>
                                                            <div>
                                                                <label>Давхар</label>
                                                                <h5>9/26</h5>
                                                            </div>
                                                            <div>
                                                                <label>Үнэ</label>
                                                                <h3>506,250,000<span>сая</span></h3>
                                                            </div>
                                                            <div>
                                                                <label>Ашиглалтад орох</label>
                                                                <h5>2023 оны 3 улирал</h5>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="layout_col dg gap1_6">
                                                    <div class="room_img">
                                                        <div class="room_plan">
                                                            <img src="images/114.png" alt="" />
                                                        </div>
                                                    </div>
                                                    <div class="room_desc">
                                                        <div class="d_list dg g2 gap1_6">
                                                            <div>
                                                                <label>Талбай</label>
                                                                <h3>114.0<sup>м2</sup></h3>
                                                            </div>
                                                            <div>
                                                                <label>Давхар</label>
                                                                <h5>9/26</h5>
                                                            </div>
                                                            <div>
                                                                <label>Үнэ</label>
                                                                <h3>513,000,000<span>сая</span></h3>
                                                            </div>
                                                            <div>
                                                                <label>Ашиглалтад орох</label>
                                                                <h5>2023 оны 3 улирал</h5>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="layout_box">
                                            <div class="layout_line dg g1_30 gap1 aic" id="rooms-4">
                                                <div class="dg g40_1 gap1_6 aic">
                                                    <div class="loyout_icon"><img src="images/icon_layout_studio.svg" /></div>
                                                    <div class="dg g5 gap1 aic">
                                                        <div class="room_c">4 өрөө</div>
                                                        <h3 class="room_area">125.0-171.8<sup>м2</sup></h3>
                                                        <h3 class="room_price">563-771<span>сая</span></h3>
                                                        <div class="room_choose">3<span>загвар</span></div>
                                                        <div class="room_ count">6<span>ш</span></div>
                                                    </div>
                                                </div>
                                                <div class="__arrow dfcc"><span class="icon"><svg width="100%" height="100%" viewBox="0 0 24 24" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xml:space="preserve" style="fill-rule:evenodd;clip-rule:evenodd;stroke-linejoin:round;stroke-miterlimit:1.41421;">
                                                    <path d="M9,21C8.93,20.997 8.913,20.999 8.844,20.988C8.74,20.971 8.639,20.939 8.546,20.891C8.453,20.843 8.367,20.781 8.293,20.707C8.049,20.463 7.947,20.102 8.028,19.767C8.058,19.639 8.114,19.518 8.191,19.412C8.232,19.356 8.246,19.344 8.293,19.293L15.586,12L8.293,4.707L8.24,4.649C8.196,4.595 8.184,4.582 8.147,4.522C8.093,4.433 8.052,4.335 8.028,4.233C8.003,4.132 7.995,4.026 8.003,3.922C8.011,3.817 8.036,3.714 8.076,3.617C8.229,3.248 8.6,3 9,3C9.184,3 9.366,3.051 9.522,3.147C9.591,3.189 9.649,3.239 9.707,3.293L17.707,11.293C17.797,11.39 17.873,11.494 17.924,11.617C18.035,11.885 18.023,12.195 17.891,12.454C17.843,12.549 17.779,12.629 17.707,12.707L9.707,20.707C9.61,20.797 9.506,20.873 9.383,20.924C9.309,20.954 9.234,20.972 9.156,20.988C9.078,20.997 9.079,20.997 9,21Z" style="fill:rgb(192,198,208);fill-rule:nonzero;"/>
                                                </svg></span></div>
                                            </div>
                                            <div class="layout_rooms">
                                                <div class="layout_col dg gap1_6">
                                                    <div class="room_img">
                                                        <div class="room_plan">
                                                            <img src="images/125.png" alt="" />
                                                        </div>
                                                    </div>
                                                    <div class="room_desc">
                                                        <div class="d_list dg g2 gap1_6">
                                                            <div>
                                                                <label>Талбай</label>
                                                                <h3>125.0<sup>м2</sup></h3>
                                                            </div>
                                                            <div>
                                                                <label>Давхар</label>
                                                                <h5>20/26</h5>
                                                            </div>
                                                            <div>
                                                                <label>Үнэ</label>
                                                                <h3>562,250,000<span>сая</span></h3>
                                                            </div>
                                                            <div>
                                                                <label>Ашиглалтад орох</label>
                                                                <h5>2023 оны 3 улирал</h5>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="layout_col dg gap1_6">
                                                    <div class="room_img">
                                                        <div class="room_plan">
                                                            <img src="images/136.png" alt="" />
                                                        </div>
                                                    </div>
                                                    <div class="room_desc">
                                                        <div class="d_list dg g2 gap1_6">
                                                            <div>
                                                                <label>Талбай</label>
                                                                <h3>136.0<sup>м2</sup></h3>
                                                            </div>
                                                            <div>
                                                                <label>Давхар</label>
                                                                <h5>22/26</h5>
                                                            </div>
                                                            <div>
                                                                <label>Үнэ</label>
                                                                <h3>612,000,000<span>сая</span></h3>
                                                            </div>
                                                            <div>
                                                                <label>Ашиглалтад орох</label>
                                                                <h5>2023 оны 3 улирал</h5>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="layout_col dg gap1_6">
                                                    <div class="room_img">
                                                        <div class="room_plan">
                                                            <img src="images/171.png" alt="" />
                                                        </div>
                                                    </div>
                                                    <div class="room_desc">
                                                        <div class="d_list dg g2 gap1_6">
                                                            <div>
                                                                <label>Талбай</label>
                                                                <h3>171.8<sup>м2</sup></h3>
                                                            </div>
                                                            <div>
                                                                <label>Давхар</label>
                                                                <h5>25-26/26</h5>
                                                            </div>
                                                            <div>
                                                                <label>Үнэ</label>
                                                                <h3>771,100,000<span>сая</span></h3>
                                                            </div>
                                                            <div>
                                                                <label>Ашиглалтад орох</label>
                                                                <h5>2023 оны 3 улирал</h5>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="">
                            <div class="fix_view box_shadow">
                                <div class="d_list">
                                    <div>
                                        <label>Орон сууцны ангилал</label>
                                        <h4>Бизнес</h4>
                                    </div>
                                    <div class="dg g2 gap1_6">
                                        <div>
                                            <label>Талбай</label>
                                            <h3>41.5-171.8<sup>м2</sup></h3>
                                        </div>
                                        <div>
                                            <label>Үнэ</label>
                                            <h3>4,5<span>сая</span></h3>
                                        </div>
                                    </div>
                                    <div>
                                        <label>Ашиглалтад орох</label>
                                        <h5>2023 оны 3 улирал</h5>
                                    </div>
                                    <div class="agent_box gray_bg">
                                        <div class="dg g1_40 gap1 aic">
                                            <div class="dg g50_1 gap1 aic">
                                                <div class="d_p_img"><img src="images/ub-properties-logo.png" /></div>
                                                <div>
                                                    <h5>Борлуулалгч</h5>
                                                    <a href="company_detail.html">Ub properties LLC</a>
                                                </div>
                                            </div>
                                            <div class="agent_phone dfcc"><span></span><i>Утасны дугаар харах</i></div>
                                        </div>
                                    </div>
                                    <div class="schedule_box">
                                        <h6>Цаг товлож орон сууц үзэх</h6>
                                        <div class="dg g3 gap1">
                                            <div class="schedule_item">
                                                <p>Мяг</p>
                                                <h4>5</h4>
                                                <p>3-р сар</p>
                                            </div>
                                            <div class="schedule_item active">
                                                <p>Лха</p>
                                                <h4>6</h4>
                                                <p>3-р сар</p>
                                            </div>
                                            <div class="schedule_item">
                                                <p>Пүр</p>
                                                <h4>7</h4>
                                                <p>3-р сар</p>
                                            </div>
                                        </div>
                                        <div class="__mt1_2">
                                            <button class="btn btn_l primary __w10">Цаг товлох</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="d_sketch sketch_plan pr">
                <div class="container">
                    <div class="dm_sub_title">
                        <h3>Ерөнхий төлөвлөгөө</h3>
                        <p>River Castle орон сууцны хорооллын төлөвлөлт</p>
                    </div>
                </div>
                <div class="sketch_plan_main">
                    <svg width="100%" height="100%" viewBox="0 0 2100 914" version="1.1" class="__sketch_polygon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xml:space="preserve" style="fill-rule:evenodd;clip-rule:evenodd;stroke-linejoin:round;stroke-miterlimit:1.41421;">
                        <g id="1" transform="matrix(1,0,0,1,1,0)">
                            <path d="M1190,534L1191,181C1142.29,185.875 1093.64,189.944 1045,194L1045,207L1022,216L1025,541L1028,547C1071.41,552.074 1111.68,553.258 1154,554L1161,549L1169,549C1175.71,544.115 1182.67,539.132 1190,534Z" data-id="1" class="path_polygon" style="fill:white;"/>
                        </g>
                        <path id="2" d="M1619,159L1622,163L1606,547L1602,566L1432,578L1416,573L1408,572L1390,558L1400,203L1430,196L1430,178L1619,159Z" data-id="2" class="path_polygon" style="fill:white;"/>
                        <path id="3" d="M1191.04,522.056L1191,534C1183.67,539.132 1176.71,544.115 1170,549L1162,549L1155,554C1112.68,553.258 1072.41,552.074 1029,547L1026,541L1025.87,526.452L994,533L994,554L952,568L952,677C968.878,688.582 1018.79,711.152 1040,717C1171.28,739.959 1310.33,759.267 1454,778L1512,760C1547.17,768.342 1583.95,771.655 1622,771L1641,750L1641,627L1622,585L1622,563L1607,538L1606,547L1602,566L1432,578L1416,573L1408,572L1390,558L1390.84,526.452L1191.04,522.056Z" data-id="3" class="path_polygon" style="fill:white;"/>
                    </svg>
                    <div class="sketch_polygon_center" style="top:42.089318%;left:71.6%" data-id="2">
                        <div><span ref="1">1 өрөө байр</span>
                            <span ref="2">2 өрөө байр</span>
                            <span ref="3">3 өрөө байр</span>
                            <span ref="2">4 өрөө байр</span>
                        </div>
                    </div>
                    <div class="sketch_polygon_center" style="top:70.38838%;left:61.031733%" data-id="3">
                        <div><span ref="1">1 өрөө байр</span>
                            <span ref="2">2 өрөө байр</span>
                            <span ref="3">3 өрөө байр</span>
                            <span ref="4">4 өрөө байр</span>
                        </div>
                    </div>
                    <div class="sketch_polygon_center" style="top:42.089318%;left:52.606533%" data-id="1">
                        <div><span ref="1">1 өрөө байр</span>
                            <span ref="2">2 өрөө байр</span>
                            <span ref="3">3 өрөө байр</span>
                            <span ref="4">4 өрөө байр</span>
                        </div>
                    </div>
                    <img src="images/river_castle_sketch.png" />
                </div>
            </div>
            <div class="container">
                <div class="dg g_d_c gap2">
                    <div class="d_main_wrap">
                        <div class="dm_sub_title">
                            <h3>Тодорхойлолт</h3>
                            <p>River Castle</p>
                        </div>
                        <div class="dm_box">
                            <div class="dg g3 gap2_4">
                                <!-- <div class="__ac">aaa</div>
                                <div class="__ac">aa</div>
                                <div class="__ac">aaa</div> -->
                            </div>
                        </div>
                        <div class="__ac">
                        
                        </div>
                        <div class="dm_sub_title">
                            <h3>Байршил, тээврийн хүртээмж</h3>
                            <p>River Castle</p>
                        </div>
                        <div class="dm_box">
                            <div class="dm_content">
                                <h2>Монголд анх удаа 26 давхар, усан парктай хотхон</h2>
                                <p>МОНГОЛД АНХ УДАА 26 ДАВХАР, УСАН ПАРКТАЙ ХОТХОН Монголд анх удаа 26 давхар орон сууц үйлчилгээний зориулалттай River castle бизнес зэрэглэлийн орон сууц захиалга авч эхэллээ. River castle бизнес  зэрэглэлийн орон сууц үйлчилгээний цогцолбор нь дотроо оршин суугчдын хэрэгцээ шаардлагыг бүрэн хангасан худалдаа үйлчилгээний төвүүд болон 6 метр өндөр усан хүрхрээ,3 эгнээ замтай усан сантай</p>
                                <p>River castle бизнес зэрэглэлийн апартмент нь 1-6 давхартаа худалдаа үйлчилгээний томоохон цогцолбор, 7-24 давхартаа бизнес зэрэглэлийн орон сууцнууд, 25-26 давхартаа тансаг зэрэглэлийн пентхаусыг таны хэрэгцээ шаардлагад нийцүүлэн төлөвлөсөн.</p>
                                <h2>Худалдаа үйлчилгээний төвүүд</h2>
                                <p>River castle бизнес зэрэглэлийн апартмент нь 1-6 давхартаа худалдаа үйлчилгээний нэгдсэн цогцолбортой бөгөөд эрхэм таны хэрэгцээг хангах супермаркет, үйлчилгээний төвүүд,фитнесс, гоо сайхан, банк гэх мэт бүхий л үйлчилгээг нэг дор төвлөрүүлсэн. Мөн чөлөөт цагаа өнгөрүүлэх үйлчилгээний хэсэгт архитектурын гоёмсог шийдэл бүхий өдөр шөнийн тэнгэрийг тольдон харах боломжтой Skylight хэсэгтэй.</p>
                                <ol>
                                    <li>/6метр өндөр хүрхрээ бүхий усан паркад эрхэм та гэр бүлээрээ тухлан цагийг өнгөрүүлэх боломжтой/</li>
                                    <li>/25 метрийн урттай 3 эгнээ замтай  халуун, хүйтэн жакузитай усан бассейнд сэлж бие бялдраа чийрэгжүүлэхээс гадна, гэр бүл,  найз нөхөдтэйгөө цагийг өнгөрүүлэх  усан  хүрхрээ бүхий  усан   парктай./</li>
                                    <li>/Эрүүл чанартай, өргөн хэрэглээний бүх барааг нэг дор төвлөрүүлсэн томоохон супермаркеттай./</li>
                                    <li>/Эрхэм та орон сууцнаасаа гаралгүйгээр хамгийн сүүлийн үеийн иж бүрэн тоног төхөөрөмжөөр тоноглогдсон Фитнес зааланд дасгал хөдөлгөөн хийж, эрүүл амьдралын  хэв  маягийг хэвшил болгох  боломжтой./</li>
                                    <li>/Орчин үеийн контемпорари интерьертэй, тансаг, тав тухыг мэдрүүлэх орчинтой ресторанаар үйлчлүүлэн, арга хэмжээ, баяр ёслол тэмдэглэх боломжтой./  </li>
                                    <li>/Үйлчилгээний төвд байрлах мини кино театр нь хамгийн сүүлийн үеийн системүүдээр тоноглогдсон, дуу дүрсний өндөр чанартай, тав  тухтай, бөгөөд таныг орчин үеийн дэлхийн кино ертөнцөөр аялуулах болно./</li>
                                </ol>
                                <h2>ДАЙМОНД АНГИЛЛЫН 4-ӨРӨӨ 171МКВ ОРОН СУУЦНЫ ТАНИЛЦУУЛГА</h2>
                                <p>Эрхэм та дэлхийд чанараар тэргүүлэгч шилдэг брэндүүдээр тоноглосон, тайван орчин, тав тухыг мэдрүүлэх Crystal, Diamond, Penthouse ангиллын дээд зэрэглэлийн орон сууцнуудаас сонголтоо  хийх  боломжтой.</p>
                                <p>Crystal ангиллын орон сууц нь 45-147 м2 талбай бүхий 1-3 өрөөний сонголттой.</p>
                                <p>Diamond ангиллын орон сууц нь 119м2-168м2 талбай бүхий  4 өрөөний сонголттой</p>
                                <p>Penthouse 24, 25-р давхруудад байрлах тансаг зэрэглэлийн орон сууц нь 100-300 м2 талбайн сонголттой.</p>
                                <ol>
                                    <li>/ Дэвшилтэт технологи, дэлхийн тэргүүлэгч брэндүүдээр  тоноглогдсон 171,8 мкв 4-өрөө орон сууцны зочны өрөө./</li>
                                    <li>/Тав тухтай орчин, төгс шийдэл бүхий өрөөний тоноглол, тохижилт бүхэн нь орчин үеийн хөгжил дэвшилтэй хөл нийлүүлэн яваа таны амжилтыг илтгэнэ./</li>
                                    <li>/Архитектур, интерьерийн төгс шийдэлтэй 171,8 мкв 4-өрөө орон сууцны унтлагын өрөө/</li>
                                    <li>/Таны тав тухыг бүрэн хангах шинэлэг шийдэл бүхий ариун цэврийн өрөөний хэсэгтэй/</li>
                                    <li>/Шинэлэг шийдэл шинэ төлөвлөлт бүхий 171,8мкв 4-өрөө орон сууцны зохион байгуулалтын  план зураг/</li>
                                </ol>
                            </div>
                            <iframe class="description_video" allowfullscreen="" src="https://www.youtube.com/embed/ciV8B9OkMV8?rel=0" frameborder="0"></iframe>
                        </div>
                    </div>
                    <div>
                        <div class="fix_view box_shadow">
                            <div class="d_list">
                                <div>
                                    <label>Орон сууцны ангилал</label>
                                    <h4>Бизнес</h4>
                                </div>
                                <div class="dg g2 gap1_6">
                                    <div>
                                        <label>Талбай</label>
                                        <h3>41.5-171.8<sup>м2</sup></h3>
                                    </div>
                                    <div>
                                        <label>Үнэ</label>
                                        <h3>4,5<span>сая</span></h3>
                                    </div>
                                </div>
                                <div>
                                    <label>Ашиглалтад орох</label>
                                    <h5>2023 оны 3 улирал</h5>
                                </div>
                                <div class="agent_box gray_bg">
                                    <div class="dg g1_40 gap1 aic">
                                        <div class="dg g50_1 gap1 aic">
                                            <div class="d_p_img"><img src="images/ub-properties-logo.png" /></div>
                                            <div>
                                                <h5>Борлуулалгч</h5>
                                                <a href="company_detail.html">Ub properties LLC</a>
                                            </div>
                                        </div>
                                        <div class="agent_phone dfcc"><span></span><i>Утасны дугаар харах</i></div>
                                    </div>
                                </div>
                                <div class="schedule_box">
                                    <h6>Цаг товлож орон сууц үзэх</h6>
                                    <div class="dg g3 gap1">
                                        <div class="schedule_item">
                                            <p>Мяг</p>
                                            <h4>5</h4>
                                            <p>3-р сар</p>
                                        </div>
                                        <div class="schedule_item active">
                                            <p>Лха</p>
                                            <h4>6</h4>
                                            <p>3-р сар</p>
                                        </div>
                                        <div class="schedule_item">
                                            <p>Пүр</p>
                                            <h4>7</h4>
                                            <p>3-р сар</p>
                                        </div>
                                    </div>
                                    <div class="__mt1_2">
                                        <button class="btn btn_l primary __w10">Цаг товлох</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
