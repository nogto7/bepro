<?php
define('DS',DIRECTORY_SEPARATOR);
define('PAGE',dirname(__FILE__).DS);
class Loader{
	private $ext			= '.php';
	private $aCss			= [];
	private $aJs			= ['pre'=>[],'after'=>[]];
	private $aMeta			= [];
	private $title			= '';
	private $sUrlSep		= ':';
	public function __construct(){
		ob_start();
		$isSecure=isset($_SERVER['HTTPS'])&&(strtolower($_SERVER['HTTPS'])=='on'||$_SERVER['HTTPS']==1);
		if(!$isSecure) $isSecure=isset($_SERVER['HTTP_X_FORWARDED_PROTO'])&&$_SERVER['HTTP_X_FORWARDED_PROTO']=='https';
		$this->protocol='http'.($isSecure?'s':'').'://';
		$this->host=$_SERVER['HTTP_HOST'];
		$a=explode('.',$this->host);
		$iDomains=count($a);
		$this->domain=join('.',array_slice($a,-2));
		$this->subdomain='';
		if($iDomains>2){
			if($a[0]=='www'){array_shift($a);$iDomains--;}
			$tmp=array_slice($a,0,$iDomains-2);
			$a=$tmp;
			$this->subdomain=join('.',$a);
		}
		$this->folder=str_replace('/index.php','',$_SERVER['SCRIPT_NAME']);
		$this->base=trim($this->protocol.$this->host.$this->folder,'/ ').'/';
		$sUrl=substr($_SERVER['REQUEST_URI'],strlen($this->folder));
		$a=[];
		if(strpos($sUrl,'?')!==false){
			list($sUrl,$get)=explode('?',$sUrl,2);
			if($get){
				$m=explode('&',$get);
				foreach($m as $s){
					list($key,$val)=explode('=',$s,2);
					if(!$val) $a[]=$key;
					else $a[$key]=$val;
				}
			}
		}
		$aParts=explode('/',trim($sUrl,'/ '));
		$this->curr=ltrim($sUrl,'/').(isset($get)&&$get?'?'.$get:'');
		$this->full=$this->base.$this->curr;
		$aDirs=[];
		$b=[];
		foreach($aParts as $part){
			$pos=strpos($part,$this->sUrlSep);
			if($pos!==false&&$pos>0){
				list($p,$v)=explode($this->sUrlSep,$part,2);
				$b[$p]=$v;
			}
			else $aDirs[]=$part;
		}
		$this->dirs=$aDirs;
		$this->path=join('/',$aDirs);
		$this->last=$aDirs?$aDirs[count($aDirs)-1]:'';
		$this->first=array_shift($aDirs);
		unset($aDirs,$aParts);
		foreach($b as $k=>$v) $a[$k]=$v;
		$this->args=$this->_trim($a);
		$this->params=$this->args;
		foreach($_POST as $k=>$v) $this->params[$k]=$v;
		$this->params=$this->_trim($this->params);
		$a=isset($_POST['do'])&&is_array($_POST['do'])?array_keys($_POST['do']):[];
		$this->action=$a?$a[0]:'no';
		$this->process();
	}
	public function process(){
		require_once PAGE.'_pre'.$this->ext;
		$p=PAGE.$this->path.$this->ext;
		if(!file_exists($p)) $p=PAGE.$this->path.DS.'index'.$this->ext;
		if(file_exists($p)) require_once $p;
		else require_once PAGE.'notfound'.$this->ext;
		require_once PAGE.'_after'.$this->ext;
		$html=ob_get_clean();
		require_once PAGE.'_tpl'.$this->ext;
	}
	public function css($s=null){
		if(is_null($s)) return $this->aCss;
		if(!in_array($s,$this->aCss)) $this->aCss[]=$s;
		return $this;
	}
	public function js($s=null,$pre=false){
		if(is_null($s)) return $this->aJs;
		elseif(is_bool($s)) return $this->aJs[$s?'pre':'after'];
		$k=$pre?'pre':'after';
		if(!in_array($s,$this->aJs[$k])) $this->aJs[$k][]=$s;
		return $this;
	}
	public function both($s){return $this->css($s)->js($s);}
	public function meta($k=null,$v=null){
		if(is_null($k)) return $this->aMeta;
		$this->aMeta[$k]=$v;
		return $this;
	}
	public function tit($s=null){if(is_null($s)) return $this->title;$this->title=$s;return $this;}
	private function _trim($m){if(is_array($m)) return array_map(['Loader','_trim'],$m);if(get_magic_quotes_gpc()) $m=stripslashes($m);return trim($m);}
}
new Loader();
?>