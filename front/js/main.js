function __ytVideo(){
    var sNews = $('.dm_box'),
        pcContWidth = $(sNews).width(),
        ytvh = ((pcContWidth * 56.25)/100),
        ytvw = ((pcContWidth * 100)/56.25),
        iFrame = $('.dm_box iframe'),
        viW = $(iFrame).width(),
        viH = $(iFrame).height();
    $(iFrame).attr('width', pcContWidth);
    if(viW > viH) {
        $(iFrame).attr('height', ytvh);
    } else {
        if($(window).innerWidth() < 821){
            $(iFrame).removeAttr('style');
            $(iFrame).attr('width', pcContWidth);
            $(iFrame).attr('height', ytvw);
        } else {
            var horz = ytvh / .8, vert = (horz * 100)/56.25;
            $(iFrame).css({display: 'table', marginLeft: 'auto', marginRight: 'auto'})
            $(iFrame).attr('width', horz);
            $(iFrame).attr('height', vert);
        }
    }
}

$(document).ready(function(){
    $('.select').niceSelect();
    __ytVideo();
    $('.rf_list > div').click(function(){
        $(this).toggleClass('active').siblings().removeClass('active');
    });
    $('.path_polygon').hover(function(){var id=$(this).addClass('show').attr('data-id');$('.sketch_polygon_center[data-id="'+id+'"]').addClass('over');},
	function(){var id=$(this).removeClass('show').attr('data-id');$('.sketch_polygon_center[data-id="'+id+'"]').removeClass('over')});
    $('.layout_line').click(function(event){
        $(this).parent().toggleClass('open').siblings().removeClass('open');
        // $(this).next().slideDown(300);
        // if($(this).parent().hasClass('open')){
        //     console.log('aaa');
        //     $(this).next().slideUp(300);
        // }
    }),
    $('.sketch_plan').click(function(e){
        var s='';
        if(e.target.tagName.toLowerCase()=='span'&&(s=$(e.target).attr('ref')||'')){
            var o=$('#rooms-'+s);
            $('html').animate({scrollTop:o.offset().top-20},600,(function(o){return function(){setTimeout(()=>{o.parent().addClass('open').siblings().removeClass('open')},100)}})(o))
        }
    });


    var thumblarge = document.getElementById('bigimg'), blur = document.getElementById('blur'), thumb = $('.thumbs > div');
    $(thumb).first().addClass('active');
    for (var i = 0; i < thumb.length; i++){
        thumb[i].onclick = function(e){
            $(thumb).removeClass('active');
            var th = this, src = $(th).children().attr("src");
            if(th){
                $(th).addClass('active');
                $(thumblarge).attr('src',src);
                $(blur).attr('src',src);
                // $('#pczvthumb img').attr('src',src);
            }
        }
    }
});;

$(window).resize(function(){
    __ytVideo();
});