function init_map(){
    var myOptions={
        zoom:16,
        scrollwheel:true,
        navigationControl: true,
        mapTypeControl: true,
        scaleControl: true,
        draggable: true,
        center:new google.maps.LatLng(47.893166, 106.8928822),
        mapTypeId:google.maps.MapTypeId.SATELLITE
    },
    map=new google.maps.Map(document.getElementById("gmap"),myOptions);
    icon = {
        url: 'lib/stat/image/maker.svg',
        scaledSize: new google.maps.Size(50, 68),
        origin: new google.maps.Point(0, 0),
        anchor: new google.maps.Point(50, 68)
    };
    marker=new google.maps.Marker({
        map:map,
        icon: icon,
        position:new google.maps.LatLng(47.893166,106.8928822)
    });
    google.maps.event.addListener(
        marker,"click",function(){
    });
};
google.maps.event.addDomListener(window,'load',init_map);